#Tuto génération de PDF avec Symfony

##Afin de générer des pdf nous avons besoin de choses à installer et configurer sur notre projet :

###Premierement (lien de la doc : https://github.com/KnpLabs/KnpSnappyBundle):
- Installer grace à composer la librairie knpSnappy dans vootre projet symfony : composer require knplabs/knp-snappy-bundle
- Choisir Oui lors de l'installation

###Deuxiemement :
- Il nous faut installer un logiciel directement sur l'ordinateur :
- https://wkhtmltopdf.org/downloads.html !!!ATTENTION pour l'installation sur Windows, installez le directement dans le C: (pas dans les programmes comme par defaut !)
- Vérifier si vous avez bien wkhtmltopdf à la racine de C:

###Troisiemement : 
- Derniere étape, lors de l'installation de KnpSnappy, vous avez eu des variables qui ont été rajouté dans le .env
- Il vous faut ajouter le chemin vers wkhtmltopdf dans la bonne variable, comme ceci : 
``WKHTMLTOPDF_PATH=C:\wkhtmltopdf\bin\wkhtmltopdf.exe``

###On est bon !
- Dans ce projet vous allez pouvoir retrouver un example complet de comment faire télécharger un pdf à un visiteur de votre site.
- Rendez vous dans le HomeController pourvoir comment je m'y prend