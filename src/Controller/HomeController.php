<?php

namespace App\Controller;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    #[Route('/download_pdf', name: 'app_download_pdf')]
    public function download_pdf(Pdf $knpSnappyPdf)
    {
        $arrayDeTest = [
            'Bonjour',
            'Aurevoir',
            'Adieu'
        ];
        //on créer un bloc html à partir d'une vue Twig que l'on stock dans la variable $html
        $html = $this->renderView('pdf/pdf_exemple.html.twig', [
            //Je peux passer des variables comme d'habitude à mon template twig
            'toto' => 'La variable toto',
            'array' => $arrayDeTest
        ]);


        //ce return ici permet de créer un téléchargement, vous pouvez bien sur mettre le nom que vous voulez de votre fichier
        return new PdfResponse(
            //cette methode de knpSnappy permet à partir d'une string avec des balises HTML de générer le pdf
            $knpSnappyPdf->getOutputFromHtml($html),
            'file.pdf'
        );
    }
}
